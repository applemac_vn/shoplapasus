﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopLapAsus.Models
{
    public class Members
    {
        public int tv_ID { get; set; }
        public string tv_User { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string tv_Pass { get; set; }

        public string tv_HoTen { get; set; }
        public string tv_DiaChi { get; set; }
        public string tv_Email { get; set; }
        public string tv_SoDienThoai { get; set; }
        public int tv_IDTypeMember { get; set; }
        public string tv_Image { get; set; }
        public int tv_Tuoi { get; set; }
    }
}