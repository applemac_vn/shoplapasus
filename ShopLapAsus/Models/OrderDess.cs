﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopLapAsus.Models
{
    public class OrderDess
    {
        public int cthd_ID { get; set; }
        public int cthd_IDUsers { get; set; }
        public Nullable<System.DateTime> cthd_ThoiGianMua { get; set; }
        public int cthd_SoLuong { get; set; }
        public int cthd_IDPr { get; set; }
        public double cthd_Tien { get; set; }
    }
}