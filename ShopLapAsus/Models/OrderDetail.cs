﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopLapAsus.Models
{
    public class OrderDetail
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public float Price { get; set; }
        public int Quality { get; set; }
        public float Total { get; set; }
             
    }
}