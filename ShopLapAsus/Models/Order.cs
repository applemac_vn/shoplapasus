﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopLapAsus.Models
{
    public class Order
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ShipAddress { get; set; }
        public string ShipName { get; set; }
        public string ShipMobile { get; set; }
        public string ShipEmail { get; set; }
        public int Status { get; set; }

    }
}