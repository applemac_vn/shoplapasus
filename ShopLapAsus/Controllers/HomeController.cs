﻿using BUS.BUS;
using ShopLapAsus.Models;
using MyConnection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopLapAsus.Controllers
{
    public class HomeController : Controller
    {
        private const string CartSession = "CartSession";
        public ActionResult Index()
        {
            return View(BUS.BUS.ProductBUS.dssp_ZenBook());
        }


        public ActionResult DetailsProD(int id)
        {
            MyConnectionDB db = new MyConnectionDB();
            return View(db.Page<MyConnection.Product>("SELECT * FROM Product WHERE sp_ID = "+id+""));
        }
        public ActionResult Details(int id)
        {
            MyConnectionDB db = new MyConnectionDB();
            return View(db.Single<MyConnection.Product>("WHERE sp_ID = " + id + "", 123));
        }

        public PartialViewResult HeaderCart()
        {
            var cart = Session[CartSession];
            var list = new List<CartItem>();
            if (cart != null)
            {
                list = (List<CartItem>)cart;
            }
            return PartialView(list);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}