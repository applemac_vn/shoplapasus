﻿using MyConnection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace ShopLapAsus.Controllers
{
    public class SearchController : Controller
    {
        public ActionResult Search(string keyword, int? page)
        {
            
            if (page == null)
                page = 1;
            MyConnectionDB db = new MyConnectionDB();
            Page<Product> dsspSearch = db.Page<Product>(page.Value, 4, "select * from Product where sp_Ten LIKE '%" +  keyword + " %'");
            return View(dsspSearch);

        }

        public ActionResult Details(int id)
        {
            MyConnectionDB db = new MyConnectionDB();
            return View(db.Single<Product>("WHERE sp_ID = " + id + "", 123));
        }
        // GET: Search
        public ActionResult Index()
        {
            return View();
        }

        // GET: Search/Details/5
       

        // GET: Search/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Search/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Search/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Search/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Search/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Search/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
