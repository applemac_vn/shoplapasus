﻿using BUS.BUS;
using MyConnection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ShopLapAsus.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Product
        public ActionResult Index(int? page)
        {
            if (page == null)
                page = 1;
            MyConnectionDB db = new MyConnectionDB();
            Page<Product> dssp = db.Page<Product>(page.Value,4,"select * from Product");
            return View(dssp);
        }

        public ActionResult Zenbook(int? page)
        {
            if (page == null)
                page = 1;
            MyConnectionDB db = new MyConnectionDB();
            Page<Product> dsspZenbook = db.Page<Product>(page.Value, 4, "select * from Product where sp_IDLoai = 1");
            return View(dsspZenbook);
        }

        public ActionResult Gaming(int? page)
        {
            if(page == null)
                page = 1;
                MyConnectionDB db = new MyConnectionDB();
                Page<Product> dsspGaming = db.Page<Product>(page.Value, 4, "select * from Product where sp_IDLoai = 2");
                return View(dsspGaming);
        }

        public ActionResult N_Series(int? page)
        {
            if (page == null)
                page = 1;
            MyConnectionDB db = new MyConnectionDB();
            Page<Product> dsspN = db.Page<Product>(page.Value, 4, "select * from Product where sp_IDLoai = 3");
            return View(dsspN);
        }

        public ActionResult KA_Serries(int? page)
        {
            if (page == null)
                page = 1;
            MyConnectionDB db = new MyConnectionDB();
            Page<Product> dsspKA = db.Page<Product>(page.Value, 4, "select * from Product where sp_IDLoai = 4");
            return View(dsspKA);
        }

        public ActionResult X_Series(int? page)
        {
            if (page == null)
                page = 1;
            MyConnectionDB db = new MyConnectionDB();
            Page<Product> dsspX = db.Page<Product>(page.Value, 4, "select * from Product where sp_IDLoai = 5");
            return View(dsspX);
        }

        public ActionResult P_Series(int? page)
        {
            if (page == null)
                page = 1;
            MyConnectionDB db = new MyConnectionDB();
            Page<Product> dsspP = db.Page<Product>(page.Value, 4, "select * from Product where sp_IDLoai = 6");
            return View(dsspP);
        }

        public ActionResult E_Series(int? page)
        {
            if (page == null)
                page = 1;
            MyConnectionDB db = new MyConnectionDB();
            Page<Product> dsspE = db.Page<Product>(page.Value, 4, "select * from Product where sp_IDLoai = 7");
            return View(dsspE);
        }

        public ActionResult V_Series(int? page)
        {
            if (page == null)
                page = 1;
            MyConnectionDB db = new MyConnectionDB();
            Page<Product> dsspV = db.Page<Product>(page.Value, 4, "select * from Product where sp_IDLoai = 8");
            return View(dsspV);
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            MyConnectionDB db = new MyConnectionDB();
            return View(db.Single<Product>("WHERE sp_ID = " + id + "", 123));
        }
        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
