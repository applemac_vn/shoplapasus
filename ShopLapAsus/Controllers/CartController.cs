﻿using BUS.BUS;
using MyConnection;
using ShopLapAsus.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ShopLapAsus.Controllers
{
    
    public class CartController : Controller
    {
        private const string CartSession = "CartSession";
        // GET: Cart
        public ActionResult Index()
        {
            var cart = Session[CartSession];
            var list = new List<CartItem>();
            if(cart != null)
            {
                list = (List<CartItem>)cart;
            }
            return View(list);
        }

        public ActionResult AddItem(int productID, int Quantity)
        {
            var product = new Models.ProductBUS().ViewDetail(productID);
            var cart = Session[CartSession];
            if(cart != null)
            {
                var list = (List<CartItem>)cart;
                if(list.Exists(x=>x.Product.sp_ID == productID))
                {
                    foreach(var item in list)
                    {
                        if(item.Product.sp_ID == productID)
                        {
                            item.Quantity += Quantity;
                        }
                    }
                }
                else
                {
                    var item = new CartItem();
                    item.Product = product;
                    item.Quantity = Quantity;
                    list.Add(item);
                }
                Session[CartSession] = list;
            }
            else
            {
                var item = new CartItem();
                item.Product = product;
                item.Quantity = Quantity;
                var list = new List<CartItem>();
                list.Add(item);
                Session[CartSession] = list;
            }
            return RedirectToAction("Index");
            
        }

        public JsonResult DeleteAll()
        {
            Session[CartSession] = null;
            return Json(new
            {
                status = true
            });
        }

        public JsonResult Delete(long id)
        {
            var sessionCart = (List<CartItem>)Session[CartSession];
            sessionCart.RemoveAll(x => x.Product.sp_ID == id);
            Session[CartSession] = sessionCart;
            return Json(new
            {
                status = true
            });
        }

        public JsonResult Update(string cartModel)
        {
            var jsonCart = new JavaScriptSerializer().Deserialize<List<CartItem>>(cartModel);
            var sessionCart = (List<CartItem>)Session[CartSession];

            foreach (var item in sessionCart)
            {
                var jsonItem = jsonCart.SingleOrDefault(x => x.Product.sp_ID == item.Product.sp_ID);
                if (jsonItem != null)
                {
                    item.Quantity = jsonItem.Quantity;
                }
            }
            Session[CartSession] = sessionCart;
            return Json(new
            {
                status = true
            });
        }
        [Authorize]
        [HttpGet]
        public ActionResult Payment()
        {
            var cart = Session[CartSession];
            var list = new List<CartItem>();
            if (cart != null)
            {
                list = (List<CartItem>)cart;
            }
            return View(list);
        }

        [HttpPost]
        public ActionResult Payment(string shipName,string mobile,string address,string email)
        {
            Models.Order order = new Models.Order();
            
            order.CreatedDate = DateTime.Now;
            order.ShipAddress = address;
            order.ShipMobile = mobile;
            order.ShipName = shipName;
            order.ShipEmail = email;
            
            var Result = MyConnectionDB.GetInstance().Insert(order);
            var cart = (List<CartItem>)Session[CartSession];
            if(cart != null)
            { 
            foreach(var item in cart)
            {
                Models.OrderDetail orderdetail = new Models.OrderDetail();
                orderdetail.ProductID = item.Product.sp_ID;
                orderdetail.Price = item.Product.sp_GiaBan;
                orderdetail.Quality = item.Quantity;
                orderdetail.Total = (item.Product.sp_GiaBan * item.Quantity);
                var Result1 = MyConnectionDB.GetInstance().Insert(orderdetail);
            }
            }
            Session[CartSession] = null;

            return Redirect("/Cart/Succes");
            
        }

        public ActionResult Succes()
        {
            return View();
        }

    }
}