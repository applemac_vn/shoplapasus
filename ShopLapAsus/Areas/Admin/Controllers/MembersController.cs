﻿using MyConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopLapAsus.Areas.Admin.Controllers
{
    public class MembersController : Controller
    {
        // GET: Admin/Members
        public ActionResult Index()
        {
            MyConnectionDB db = new MyConnectionDB();
            return View(db.Page<Models.Members>("select * from Members"));
        }

        // GET: Admin/Members/Details/5
        public ActionResult Details(int id)
        {
            MyConnectionDB db = new MyConnectionDB();
            return View(db.Single<Models.Members>("where tv_ID = "+id+""));
        }

        // GET: Admin/Members/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Members/Create
        [HttpPost]
        public ActionResult Create(Member mr)
        {
            try
            {
                // TODO: Add insert logic here
                var Result = MyConnectionDB.GetInstance().Insert(mr);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Members/Edit/5
        public ActionResult Edit(int id)
        {
            MyConnectionDB db = new MyConnectionDB();
            return View(db.Single<Member>("where tv_ID = "+id+"",123));
        }

        // POST: Admin/Members/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Member mr)
        {
            try
            {
                // TODO: Add update logic here
                var result = MyConnectionDB.GetInstance().Update(mr);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Members/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Members/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
