﻿using BUS.BUS;
using MyConnection;
//using ShopLapAsus.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopLapAsus.Areas.Admin.Controllers
{
    public class ProductController : Controller
    {
        // GET: Admin/Product
        public ActionResult Index()
        {
            return View(ProductBUS.dssp());
        }

        // GET: Admin/Product/Details/5
        public ActionResult Details(int id)
        {
            MyConnectionDB db = new MyConnectionDB();
            return View(db.Single<Product>("WHERE sp_ID = "+id+"",123));
        }

        // GET: Admin/Product/Create
        public ActionResult Create()
        {
            
            return View();
        }

        // POST: Admin/Product/Create
        [HttpPost]
        public ActionResult Create(Product pro, HttpPostedFileBase file)
        {
            var validImageTypes = new string[]
    {
        "image/gif",
        "image/jpeg",
        "image/pjpeg",
        "image/png"
    };

            if (file == null || file.ContentLength == 0)
            {
                ModelState.AddModelError("ImageUpload", "This field is required");
            }
            else if (!validImageTypes.Contains(file.ContentType))
            {
                ModelState.AddModelError("ImageUpload", "Please choose either a GIF, JPG or PNG image.");
            }

            if (ModelState.IsValid)
            {

                if (file != null && file.ContentLength > 0)
                {
                    var uploadDir = "~/Content/images/upload/";
                    var imagePath = Path.Combine(Server.MapPath(uploadDir),file.FileName);
                    //var imageUrl = Path.Combine(uploadDir, file.FileName);
                    file.SaveAs(imagePath);
                    pro.sp_HinhAnh = "/Content/images/upload/" + file.FileName;
                    //image.ImageUrl = imageUrl;
                }
            }
            // TODO: Add insert logic here
            var Result = MyConnectionDB.GetInstance().Insert(pro);
                return RedirectToAction("Index");
                         
            
        }

        // GET: Admin/Product/Edit/5
        public ActionResult Edit(int id)
        {
            MyConnectionDB db = new MyConnectionDB();

            return View(db.Single<Product>("where sp_ID = "+id+""));
        }

        // POST: Admin/Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Product pro)
        {
            try
            {
                // TODO: Add update logic here
                var Result = MyConnectionDB.GetInstance().Update(pro);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Product/Delete/5
        public ActionResult Delete(int id)
        {
            MyConnectionDB db = new MyConnectionDB();
            return View(db.Single<Product>("WHERE sp_ID = "+id+""));
        }

        // POST: Admin/Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Product pro)
        {
            try
            {
                // TODO: Add delete logic here
                MyConnectionDB db = new MyConnectionDB();
                db.Delete<Product>("WHERE sp_ID = "+id+"");
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
