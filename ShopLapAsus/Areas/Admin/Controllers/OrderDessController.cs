﻿using MyConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopLapAsus.Areas.Admin.Controllers
{
    public class OrderDessController : Controller
    {
        // GET: Admin/OrderDess
        public ActionResult Index()
        {
            MyConnectionDB db = new MyConnectionDB();
            return View(db.Page<OrderDess>("select * from OrderDess"));
        }

        // GET: Admin/OrderDess/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/OrderDess/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/OrderDess/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/OrderDess/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/OrderDess/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/OrderDess/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/OrderDess/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
