﻿var Search = {
    init: function () {
        Search.regEvents();
    },
    regEvents: function () {
        $('#txtKeyword').off('enter').on('enter', function () {
            window.location.href = "/"
        });
    }
}