﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS.Models
{
    public class Product
    {

        public int sp_ID { get; set; }
        public string sp_Ten { set; get; }
        public int sp_IDLoai { get; set; }
        public int sp_IDTinhTrang { get; set; }
        public long sp_GiaBan { get; set; }
        public long sp_GiamGia { get; set; }
        public string sp_MoTa { get; set; }
        public Nullable<System.DateTime> sp_NgayNhap { get; set; }
        public int sp_SoLuongTon { get; set; }
        public int sp_SoLuongBan { get; set; }
        public string sp_HinhAnh { get; set; }
        public int sp_ManHinh { get; set; }
        public int sp_HDD { get; set; }
        public string sp_ODia { get; set; }
        public string sp_BoXuLy { get; set; }
        public string sp_DoMong { get; set; }
    }
}
