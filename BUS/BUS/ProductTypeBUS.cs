﻿using MyConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS.BUS
{
    public class ProductTypeBUS
    {
        public static IEnumerable<ProductType> dslsp()
        {
            return MyConnectionDB.GetInstance().Page<ProductType>("SELECT * FROM ProductType");
        }
    }
}
