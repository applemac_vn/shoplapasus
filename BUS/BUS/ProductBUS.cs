﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyConnection;

namespace BUS.BUS
{
    public class ProductBUS
    {
        public static IEnumerable<Product> dssp()
        {
            MyConnectionDB db = new MyConnectionDB();
            return db.Page<Product>("SELECT * FROM Product");
        }
        public static IEnumerable<Product> dssp_ZenBook()
        {
            MyConnectionDB db = new MyConnectionDB();
            return db.Page<Product>("SELECT top 3 * FROM Product WHERE sp_IDLoai = 1");
        }
        public static IEnumerable<Product> dssp_ROG()
        {
            MyConnectionDB db = new MyConnectionDB();
            return db.Page<Product>("SELECT * FROM Product WHERE sp_IDLoai = 2");
        }
        public static IEnumerable<Product> dssp_N()
        {
            MyConnectionDB db = new MyConnectionDB();
            return db.Page<Product>("SELECT * FROM Product WHERE sp_IDLoai = 3");
        }
        public static IEnumerable<Product> dssp_E()
        {
            MyConnectionDB db = new MyConnectionDB();
            return db.Page<Product>("SELECT * FROM Product WHERE sp_IDLoai = 7");
        }
        public Product ViewDetail(int id)
        {
            MyConnectionDB db = new MyConnectionDB();
            return db.Single<Product>("where sp_ID = "+id+"");
            //return db.Page<Product>("SELECT * FROM Product WHERE sp_ID = " + id + "");
        }

        public static IEnumerable<Product> dsspLQ(int id)
        {
            
            MyConnectionDB db = new MyConnectionDB();
            return db.Page<Product>("SELECT * FROM Product WHERE sp_IDLoai ="+id+"");
        }
    }
}
